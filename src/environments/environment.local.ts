export const environment = {
    production: false,
    app: {
        client_url: 'http://local.j-intuitive-simple-pos-and-inventory-system.com/',
        api_url: 'http://local-api.j-intuitive-simple-pos-and-inventory-system.com'
    }
};
