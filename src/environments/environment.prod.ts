export const environment = {
    production: true,
    app: {
        client_url: 'http://local.j-intuitive-simple-pos-and-inventory-system.com/',
        api_url: 'http://posapi.officialnusg.com'
    }
};
