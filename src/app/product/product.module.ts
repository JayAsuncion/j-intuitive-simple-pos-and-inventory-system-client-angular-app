import {NgModule} from "@angular/core";
import {ProductsService} from "./services/products.service";
import {ProductListComponent} from "./components/product-list.component";
import {ProductRoutes} from "./product.routes";
import {CommonModule} from "@angular/common";
import {ProductItemFormComponent} from "./components/product-item-form.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AppSharedModule} from "../shared/shared.module";
import {ProductStocksService} from "./services/product-stocks.service";

@NgModule({
    imports: [
        ProductRoutes,
        CommonModule,
        ReactiveFormsModule,
        AppSharedModule,
        FormsModule
    ],
    declarations: [
        ProductListComponent,
        ProductItemFormComponent
    ],
    providers: [
        ProductsService,
        ProductStocksService
    ],
    exports: [

    ]
})
export class ProductModule {}
