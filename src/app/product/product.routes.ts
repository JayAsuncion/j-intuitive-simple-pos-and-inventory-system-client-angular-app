import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ProductListComponent} from "./components/product-list.component";
import {ProductItemFormComponent} from "./components/product-item-form.component";

const routes: Routes = [
    {
        path: '',
        component: ProductListComponent,
        data: {
            pageTitle: 'Product list',
            pageRoute: '/product-list',
            showHeader: true,
            showNav: true
        }
    },
    {
        path: 'product/new',
        component: ProductItemFormComponent,
        data: {
            pageTitle: 'Product Add Form',
            pageRoute: '/product/new',
            showHeader: true,
            showNav: true
        }
    },
    {
        path: 'product/:product_id/edit',
        component: ProductItemFormComponent,
        data: {
            pageTitle: 'Product Item',
            pageRoute: '/product-item',
            showHeader: true,
            showNav: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProductRoutes {}
