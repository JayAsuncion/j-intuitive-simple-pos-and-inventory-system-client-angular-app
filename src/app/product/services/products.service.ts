import {Injectable, OnDestroy} from "@angular/core";
import {Observable, Subject} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: "root"
})
export class ProductsService implements OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url + '/products';

    constructor(private httpClient: HttpClient) {
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    public getItem(search_value, search_field, options = {}): Observable<any> {
        let queryParams = '';
        queryParams = queryParams + `search_field=${search_field}`;

        Object.keys(options).forEach((key, index) => {
            queryParams = queryParams + `&${key}=${options[key]}`;
        });

        return this.httpClient.get(this.resourceUrl + `/${search_value}?${queryParams}`);
    }

    public getProductsAndProductStocks(): Observable<any> {
        return this.httpClient.get(this.resourceUrl);
    }

    public getLowProductStocks(): Observable<any> {
        return this.httpClient.get(this.resourceUrl + '?quantity_level=low');
    }

    public getHighProductStocks(): Observable<any> {
        return this.httpClient.get(this.resourceUrl + '?quantity_level=high');
    }

    public getProductsAndProductStocksByColumn(columnName, columnValue): Observable<any> {
        return this.httpClient.get(this.resourceUrl + `?search_field=${columnName}&search_keyword=${columnValue}`);
    }

    public createItem(formData): Observable<any> {
        return this.httpClient.post(this.resourceUrl, formData);
    }

    public updateItem(product_id, formData) {
        return this.httpClient.put(this.resourceUrl + `/${product_id}`, formData);
    }

    public updateProductImageURL(product_id, imageUrl): Observable<any> {
        const formData = {image_url: imageUrl};

        return this.httpClient.put(this.resourceUrl + `/${product_id}?action=UPDATE_IMAGE_URL`, formData);
    }
}
