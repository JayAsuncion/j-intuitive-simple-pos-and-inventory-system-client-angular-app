import {Injectable, OnDestroy} from "@angular/core";
import {Observable, Subject} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: "root"
})
export class ProductStocksService implements OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url + '/product-stocks';

    constructor(private httpClient: HttpClient) {
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    public updateProductStockItem(product_id, formData) {
        return this.httpClient.put(this.resourceUrl + `/${product_id}`, formData);
    }
}
