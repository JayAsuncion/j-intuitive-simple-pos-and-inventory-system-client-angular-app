import {Component, OnDestroy, OnInit} from "@angular/core";
import {ProductsService} from "../services/products.service";
import {takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";
import {$t} from "codelyzer/angular/styles/chars";

@Component({
    selector: 'app-product-list',
    templateUrl: 'product-list.component.html',
    styleUrls: [
        'product-list.component.less'
    ]
})
export class ProductListComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    public productList = [];
    public showLoader = false;

    public productNameSearchKeyword = '';
    public showFilterLoader = false;
    private keyCodeMaps = {
        Enter: 13
    };

    constructor(
        private ProductsService: ProductsService
    ) {
    }

    ngOnInit() {
        this.showLoader = true;
        this.ProductsService.getProductsAndProductStocks()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                console.log(response)
                this.productList = response.data.products;
                this.showLoader = false;
            });
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    setStockBadgeClass(product) {
        const lowStockCount = parseFloat(product.low_stock_count);
        const highStockCount = parseFloat(product.high_stock_count)
        const stock = parseFloat(product.quantity);

        if (stock <= lowStockCount) {
            return 'low';
        }

        if (stock >= highStockCount) {
            return 'high';
        }

        return 'medium';
    }

    setProductImage(product) {
        if (product.image_url) {
            return product.image_url;
        }

        return 'assets/img/product.png';
    }

    productNameKeypressHandler(event) {
        if (event.keyCode === this.keyCodeMaps.Enter) {
            this.search();
        }
    }

    search() {
        const searchKeyword = this.productNameSearchKeyword;

        if (this.showFilterLoader) {
            return;
        }

        this.showFilterLoader = true;

        this.ProductsService.getProductsAndProductStocksByColumn('product_name', searchKeyword)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                console.log(response)
                this.productList = response.data.products;
                this.showFilterLoader = false;
            });
    }
}
