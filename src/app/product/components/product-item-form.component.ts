import {Component, OnDestroy, OnInit} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {ProductsService} from "../services/products.service";
import {ActivatedRoute, Router} from "@angular/router";
import {takeUntil} from "rxjs/operators";
import {SimpleModalService} from "ngx-simple-modal";
import {AppSimpleAlertModalComponent} from "../../shared/components/app-simple-alert-modal.component";

@Component({
    selector: 'app-product-item-form',
    templateUrl: 'product-item-form.component.html',
    styleUrls: [
        'product-item-form.component.less'
    ]
})
export class ProductItemFormComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();

    public formMode = 'create';
    public itemForm: FormGroup;

    public IDFromRoute;
    public isItemLoaded = false;
    public itemData = {
        barcode: '',
        product_name: '',
        unit: '',
        unit_price: 0,
        unit_cost: 0,
        low_stock_count: 0,
        high_stock_count: 0,
        image_url: ''
    };
    public isFormValid = true;
    public isItemFormSubmitted = false;

    public panelTitle = 'Basic Information';
    public activePanel = 'basic';

    public productImage = [];

    constructor(
        private ProductsService: ProductsService,
        private ActivatedRoute: ActivatedRoute,
        private SimpleModalService: SimpleModalService,
        private Router: Router
    ) {
    }

    ngOnInit() {
        this.IDFromRoute = this.ActivatedRoute.snapshot.params.product_id;
        this.formMode = this.IDFromRoute ? 'update' : 'create';

        this.buildItemForm();


        if (this.formMode === 'update') {
            this.ProductsService.getItem(this.IDFromRoute, 'product_id')
                .pipe(takeUntil(this.destroyed$))
                .subscribe(response => {
                    this.itemData = response.data.product;
                    this.patchFormData();
                });
        }
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    buildItemForm() {
        this.itemForm = new FormGroup({
            barcode: new FormControl('', []),
            product_name: new FormControl('', [Validators.required]),
            unit: new FormControl('', [Validators.required]),
            unit_price: new FormControl(0, [Validators.required]),
            unit_cost: new FormControl(0, [Validators.required]),
            low_stock_count: new FormControl(0, [Validators.required]),
            high_stock_count: new FormControl(0, [Validators.required]),
        });
    }

        patchFormData() {
        this.itemForm.patchValue({
            barcode: this.itemData.barcode,
            product_name: this.itemData.product_name,
            unit: this.itemData.unit,
            unit_price: this.itemData.unit_price,
            unit_cost: this.itemData.unit_cost,
            low_stock_count: this.itemData.low_stock_count,
            high_stock_count: this.itemData.high_stock_count
        });

        if (this.itemData.image_url.length > 0) {
            this.productImage.push({url: this.itemData.image_url});
        }
    }

    formAction() {
        if (this.activePanel === 'basic') {
            this.isFormValid = this.itemForm.valid;

            if (!this.isFormValid) {
                console.log('Form Invalid', this.itemForm.errors)
                return;
            }

            if (this.formMode === 'create') {
                if (this.isItemFormSubmitted) {
                    return;
                }

                this.isItemFormSubmitted = true;

                const form_data = this.itemForm.value;
                this.ProductsService.createItem(form_data)
                    .pipe(takeUntil(this.destroyed$))
                    .subscribe(response => {
                        this.isItemFormSubmitted = false;

                        this.SimpleModalService.addModal(AppSimpleAlertModalComponent, {
                            title: 'Success',
                            message: 'Product successfully created',
                            buttonText: 'Okay'
                        }, {
                            closeOnClickOutside: true
                        });

                        this.Router.navigate(['product/' + response.data.product_id + '/edit']);
                    });
            } else if (this.formMode === 'update') {
                if (this.isItemFormSubmitted) {
                    return;
                }

                this.isItemFormSubmitted = true;
                const product_id = this.IDFromRoute;
                const form_data = this.itemForm.value;

                this.ProductsService.updateItem(product_id, form_data)
                    .pipe(takeUntil(this.destroyed$))
                    .subscribe(response => {
                        this.isItemFormSubmitted = false;

                        this.SimpleModalService.addModal(AppSimpleAlertModalComponent, {
                            title: 'Success',
                            message: 'Product successfully updated',
                            buttonText: 'Okay'
                        }, {
                            closeOnClickOutside: true
                        });
                    });
            }
        } else {
            if (this.formMode === 'create') {
                console.log('create')
            } else if (this.formMode === 'update') {
                if (this.productImage && this.productImage.length > 0) {
                    this.ProductsService.updateProductImageURL(this.IDFromRoute, this.productImage[0].url)
                        .pipe(takeUntil(this.destroyed$)).subscribe(response => {
                        console.log('response', response)
                        this.SimpleModalService.addModal(AppSimpleAlertModalComponent, {
                            title: 'Success',
                            message: 'Product image successfully updated',
                            buttonText: 'Okay'
                        }, {
                            closeOnClickOutside: true
                        });
                    });
                } else {
                    this.SimpleModalService.addModal(AppSimpleAlertModalComponent, {
                        icon: 'info',
                        title: 'Alert',
                        message: 'If you want to change the product image, drag an image, then click Save.',
                        buttonText: 'Okay'
                    }, {
                        closeOnClickOutside: true
                    });
                }
            }
        }
    }

    get fC() {
        return this.itemForm.controls;
    }
}
