import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {CoreModule} from "./core/core.module";
import {AppRoutes} from "./app.routes";
import {SalesModule} from "./sales/sales.module";
import {HttpClientModule} from "@angular/common/http";
import {ProductStockModule} from "./product-stock/product-stock.module";
import {ReportsModule} from "./reports/reports.module";

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutes,
        CoreModule,
        ProductStockModule,
        SalesModule,
        ReportsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
