import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from "rxjs";
import {RouteExtractorService} from "./core/services/route-extractor.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();

    constructor(
        public RouteExtractorService: RouteExtractorService
    ) {

    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }
}
