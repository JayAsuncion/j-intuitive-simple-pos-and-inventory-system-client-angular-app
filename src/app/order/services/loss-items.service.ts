import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
    providedIn: "root"
})
export class LossItemsService {
    private resourceUrl = environment.app.api_url + '/loss-items';

    constructor(private httpClient: HttpClient) {
    }

    public createLossItem(data): Observable<any> {
        const formData = data;
        return this.httpClient.post(this.resourceUrl, formData);
    }
}
