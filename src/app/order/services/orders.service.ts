import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class OrdersService {
    private resourceUrl = environment.app.api_url + '/orders';

    constructor(private httpClient: HttpClient) {
    }

    public createOrder(data): Observable<any> {
        const formData = {
            'customer_info' : data['customer_info'],
            'cart_items' : data['cart_items']
        }
        return this.httpClient.post(this.resourceUrl, formData);
    }

    public getAllOrders(): Observable<any> {
        return this.httpClient.get(this.resourceUrl);
    }

    public getOrders(params?): Observable<any> {
        let queryParams = `?fromDateTimestamp=${params.fromDateTimestamp}&toDateTimestamp=${params.toDateTimestamp}`;
        return this.httpClient.get(this.resourceUrl + queryParams);
    }

    public voidOrder(orderID): Observable<any> {
        const orderData = {
            'delete_flag' : 'y'
        };
        return this.httpClient.put(this.resourceUrl + `/${orderID}`, orderData);
    }
}
