import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {AppSharedModule} from "../shared/shared.module";

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        AppSharedModule
    ],
    declarations: [

    ],
    providers: [

    ],
    exports: [

    ]
})
export class OrderModule {}
