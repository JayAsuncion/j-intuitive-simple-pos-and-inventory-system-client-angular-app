import {AfterViewInit, Component, ViewChild} from '@angular/core';
import { SimpleModalComponent } from 'ngx-simple-modal';


export interface SimpleAlertModalModel {
    title?: string;
    message: string;
    buttonText: string;
    icon?: string;
}

@Component({
    selector: 'app-simple-alert-modal',
    template: `
    <div class="modal-content simple-alert-modal-container">
        <div class="modal-header">
            <h2 class="modal-title-wrapper">
                <i class="modal-icon-success fa fa-check-circle" [ngClass]="{'fa-info-circle': icon === 'info'}"></i>
                <label class="modal-title">{{title || 'Alert!'}}</label>
            </h2>
        </div>
        <div class="modal-body">
            <label class="modal-messsage">{{message}}</label>
        </div>
        <div class="modal-footer">
            <button type="button" class="site-btn-success-md" (click)="buttonClick()" #modalPrimaryButton>{{buttonText}}</button>
        </div>
    </div>
  `,
    styleUrls: [
        'app-simple-alert-modal.component.less'
    ]
})
export class AppSimpleAlertModalComponent extends SimpleModalComponent<SimpleAlertModalModel, null> implements SimpleAlertModalModel, AfterViewInit {
    title: string;
    message: string;
    buttonText: string;
    icon: string;

    @ViewChild('modalPrimaryButton') modalPrimaryButton;

    constructor() {
        super();
    }

    ngAfterViewInit() {
        this.modalPrimaryButton.nativeElement.focus();
    }

    buttonClick() {
        // @ts-ignore
        this.result = {
            buttonClick: true
        };
        this.close();
    }
}
