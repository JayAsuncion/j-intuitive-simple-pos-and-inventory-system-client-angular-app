import {AfterViewInit, Component, ViewChild} from '@angular/core';
import { SimpleModalComponent } from 'ngx-simple-modal';

export interface OrderModelForModal {
    subtotal: number;
    discount: number;
    total: number;
    cash: number;
    change: number;
}

export interface OrderSummaryModel {
    title?: string;
    message: string;
    data: OrderModelForModal;
    buttonText: string;
}

@Component({
    selector: 'order-summary-modal',
    template: `
    <div class="modal-content order-summary-modal-container">
        <div class="modal-header">
            <h2 class="modal-title-wrapper">
                <i class="modal-icon-success fa fa-check-circle"></i>
                <label class="modal-title">{{title || 'Alert!'}}</label>
            </h2>
        </div>
        <div class="modal-body">
            <label class="customer-name-label">Customer Name</label>
            <h3 class="customer-name-value">Christian Jay D. Asuncion</h3>
            <section class="order-summary-wrapper">
                <h3 class="order-summary-title">
                    Summary
                </h3>
                <span class="summary-group">
                    <label>SUBTOTAL</label>
                    <label class="summary-value">₱ {{data.subtotal}}</label>
                </span>
                <span class="summary-group">
                    <label>DISCOUNT</label>
                    <label class="summary-value">₱ {{data?.discount}}</label>
                </span>
                <span class="summary-group with-top-border text-green">
                    <label><strong>TOTAL</strong></label>
                    <label class="summary-value">₱ {{data.total}}</label>
                </span>
                <span class="summary-group">
                    <label>CASH</label>
                    <label class="summary-value">₱ {{data.cash}}</label>
                </span>
                <span class="summary-group with-top-border text-orange">
                    <label><strong>CHANGE</strong></label>
                    <label class="summary-value">₱ {{data.change}}</label>
                </span>
            </section>
        </div>
        <div class="modal-footer">
            <button type="button" class="site-btn-success-md" (click)="nextCustomerModalClick()" #modalPrimaryButton>{{buttonText}}</button>
        </div>
    </div>
  `,
    styleUrls: [
        'order-summary-modal.component.less'
    ]
})
export class OrderSummaryModalComponent extends SimpleModalComponent<OrderSummaryModel, null> implements OrderSummaryModel, AfterViewInit {
    title: string;
    message: string;
    data: OrderModelForModal;
    buttonText: string;

    @ViewChild('modalPrimaryButton') modalPrimaryButton;

    constructor() {
        super();
    }

    ngAfterViewInit() {
        this.modalPrimaryButton.nativeElement.focus();
    }

    nextCustomerModalClick() {
        // @ts-ignore
        this.result = {
            nextCustomer: true
        };
        this.close();
    }
}
