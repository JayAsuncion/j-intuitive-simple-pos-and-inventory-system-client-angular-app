import { Component } from '@angular/core';
import { SimpleModalComponent } from 'ngx-simple-modal';


export interface SimpleConfirmationModalModel {
    icon?: string;
    iconClass?: string;
    title?: string;
    message: string;
    buttonText1: string;
    buttonText2: string;
}

@Component({
    selector: 'app-simple-confirmation-modal',
    template: `
    <div class="modal-content simple-confirmation-modal-container">
        <div class="modal-header">
            <h2 class="modal-title-wrapper">
                <i class="fa fa-check-circle"
                   [ngClass]="{
                        'modal-icon-success': (iconClass === undefined || iconClass === 'success'),
                        'modal-icon-warning': iconClass === 'warning',
                        'modal-icon-extra-danger': iconClass === 'extra-danger',
                        'fa-info-circle': icon === 'info'}"></i>
                <label class="modal-title">{{title || 'Alert!'}}</label>
            </h2>
        </div>
        <div class="modal-body">
            <label class="modal-messsage">{{message}}</label>
        </div>
        <div class="modal-footer">
            <button type="button" class="site-btn-extra-danger-md modal-button-primary" (click)="button1Click()">{{buttonText1}}</button>
            <button type="button" class="site-btn-warning-md modal-button-secondary" (click)="button2Click()">{{buttonText2}}</button>
        </div>
    </div>
  `,
    styleUrls: [
        'app-simple-confirmation-modal.component.less'
    ]
})
export class AppSimpleConfirmationModalComponent extends SimpleModalComponent<SimpleConfirmationModalModel, null> implements SimpleConfirmationModalModel {
    icon: string;
    iconClass: string;
    title: string;
    message: string;
    buttonText1: string;
    buttonText2: string;

    constructor() {
        super();
    }

    button1Click() {
        // @ts-ignore
        this.result = {
            confirm: true
        };
        this.close();
    }

    button2Click() {
        // @ts-ignore
        this.result = {
            confirm: false
        };
        this.close();
    }
}
