import {Component, forwardRef, OnDestroy, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {FileHandle} from '../interface/file-handler.interface';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {ImageUploaderService} from "../../core/services/image-uploader.service";


@Component({
    selector: 'app-file-drag-and-drop',
    templateUrl: 'app-file-drag-and-drop.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AppFileDragAndDropComponent),
            multi: true
        }
    ]
})
export class AppFileDragAndDropComponent implements ControlValueAccessor, OnInit, OnChanges, OnDestroy {
    private destroyed$ = new Subject();
    @Input() folder;
    @Input() maxItem;
    @Input() initialFiles;

    // Display
    @Input() emptyMessage = 'Drag and Drop Your <strong>File</strong> Here';

    public disabled: boolean;
    public value: any[] = [];

    constructor(
        private ImageUploaderService: ImageUploaderService
    ) { }

    ngOnInit(): void {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.initialFiles !== undefined && changes.initialFiles.currentValue !== null) {
            if (
                changes.initialFiles.previousValue === '' && !changes.initialFiles.firstChange ||
                !changes.initialFiles.firstChange && changes.initialFiles.currentValue.length >= 0 ||
                changes.initialFiles.firstChange && changes.initialFiles.currentValue.length > 0
            ) {
                const initialFiles = changes.initialFiles.currentValue;
                this.value = [];

                for (const file of initialFiles) {
                    console.log(initialFiles);
                    this.value.push({image_id: file.image_id, url: file.url});
                }

                this.onChanged(initialFiles);
            }
        }
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    onChanged: any = () => {};
    onTouched: any = () => {};

    writeValue(val: any): void {
        this.onChanged(val);
    }

    registerOnChange(fn: any): void {
        this.onChanged = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    filesDroppedEvent(files: FileHandle[]) {
        this.onTouched();
        console.log('files', files)
        this.ImageUploaderService.uploadImages(files, {folder: this.folder})
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    console.log('response', response);
                    const images = response.data.images;

                    for (const image of images) {
                        this.value.push({url: image.url});
                    }

                    this.onChanged(this.value);
                },
                error => {
                    console.log('filesDroppedEvent', error);
                }
            );

    }

    removeImage(index) {
        this.value.splice(index, 1);
    }
}
