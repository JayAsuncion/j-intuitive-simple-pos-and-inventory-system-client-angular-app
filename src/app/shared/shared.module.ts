import {NgModule} from "@angular/core";
import {OrderSummaryModalComponent} from "./components/order-summary-modal.component";
import {NumbersOnlyDirective} from "./directives/numbers-only.directive";
import {CommonModule} from "@angular/common";
import {AppSimpleAlertModalComponent} from "./components/app-simple-alert-modal.component";
import {AppFileDragAndDropComponent} from "./components/app-file-drag-and-drop-component";
import {AppFileDragAndDropDirective} from "./directives/app-file-drag-and-drop.directive";
import {AppSimpleConfirmationModalComponent} from "./components/app-simple-confirmation-modal.component";

@NgModule({
    imports: [
        CommonModule

    ],
    declarations: [
        OrderSummaryModalComponent,
        AppSimpleAlertModalComponent,
        AppSimpleConfirmationModalComponent,
        AppFileDragAndDropComponent,
        AppFileDragAndDropDirective,
        NumbersOnlyDirective
    ],
    providers: [

    ],
    exports: [
        OrderSummaryModalComponent,
        NumbersOnlyDirective,
        AppFileDragAndDropComponent
    ]
})
export class AppSharedModule {}
