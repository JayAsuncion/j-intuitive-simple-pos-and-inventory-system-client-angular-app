import {NgModule} from "@angular/core";
import {AppProductStockListComponent} from "./components/app-product-stock-list.component";
import {ProductStockRoutes} from "./product-stock.routes";
import {CommonModule} from "@angular/common";
import {ProductModule} from "../product/product.module";
import {AppAddProductStockComponent} from "./components/add-product-stock/app-add-product-stock.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [
        ProductStockRoutes,
        CommonModule,
        ProductModule,
        ReactiveFormsModule,
        FormsModule
    ],
    declarations: [
        AppProductStockListComponent,
        AppAddProductStockComponent
    ],
    providers: [

    ],
    exports: [
        AppProductStockListComponent
    ]
})
export class ProductStockModule {}
