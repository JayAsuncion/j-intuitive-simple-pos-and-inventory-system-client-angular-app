import {Component, OnInit} from "@angular/core";
import {ProductsService} from "../../product/services/products.service";
import {takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";

@Component({
    selector: 'app-product-stock-list',
    templateUrl: 'app-product-stock-list.component.html',
    styleUrls: [
        'app-product-stock-list.component.less'
    ]
})
export class AppProductStockListComponent implements OnInit {
    private destroyed$ = new Subject();

    public productStocks: any;
    public activeTab = 'All';
    public showLoader = false;

    public barcodeSearchKeyword = '';
    public showFilterLoader = false;

    private keyCodeMaps = {
        Enter: 13
    };

    constructor(
        private ProductsService: ProductsService
    ) {
    }

    ngOnInit() {
        this.loadAllProductStocks();
    }

    calculateNeed(product) {
        let quantity = parseFloat(product.quantity);
        let high_stock_count = parseFloat(product.high_stock_count);

        if (quantity >= high_stock_count) {
            return 0;
        }

        return high_stock_count - quantity;
    }

    setProductImage(product) {
        if (product.image_url) {
            return product.image_url;
        }

        return 'assets/img/product.png';
    }

    setStockBadgeClass(product) {
        const lowStockCount = parseFloat(product.low_stock_count);
        const highStockCount = parseFloat(product.high_stock_count)
        const stock = parseFloat(product.quantity);

        if (stock <= lowStockCount) {
            return 'low';
        }

        if (stock >= highStockCount) {
            return 'high';
        }

        return 'medium';
    }

    loadAllProductStocks() {
        this.activeTab = 'All';
        this.productStocks = [];
        this.showLoader = true;

        this.ProductsService.getProductsAndProductStocks()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                console.log(response)
                this.productStocks = response.data.products;
                this.showLoader = false;
            });
    }

    loadLowStockProducts() {
        this.activeTab = 'Low';
        this.productStocks = [];
        this.showLoader= true;

        this.ProductsService.getLowProductStocks()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                console.log(response)
                this.productStocks = response.data.products;
                this.showLoader = false;
            });

    }

    loadHighStockProducts() {
        this.activeTab = 'High';
        this.productStocks = [];
        this.showLoader = true;

        this.ProductsService.getHighProductStocks()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                console.log(response)
                this.productStocks = response.data.products;
                this.showLoader = false;
            });

    }

    search() {
        const searchKeyword = this.barcodeSearchKeyword;

        if (this.showFilterLoader) {
            return;
        }

        this.showFilterLoader = true;

        this.ProductsService.getProductsAndProductStocksByColumn('barcode', searchKeyword)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                console.log(response)
                this.productStocks = response.data.products;
                this.showFilterLoader = false;
            });
    }

    barcodeKeypressHandler(event) {
        if (event.keyCode === this.keyCodeMaps.Enter) {
            this.search();
        }
    }
}
