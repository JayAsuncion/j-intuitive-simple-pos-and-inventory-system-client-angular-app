import {AfterViewInit, Component, OnInit, ViewChild} from "@angular/core";
import {forkJoin, Subject} from "rxjs";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ProductsService} from "../../../product/services/products.service";
import {ActivatedRoute, Router} from "@angular/router";
import {map, takeUntil} from "rxjs/operators";
import {ProductStocksService} from "../../../product/services/product-stocks.service";
import {LossItemsService} from "../../../order/services/loss-items.service";

@Component({
    selector: 'app-add-product-stock',
    templateUrl: 'app-add-product-stock.component.html',
    styleUrls: [
        'app-add-product-stock.component.less'
    ]
})
export class AppAddProductStockComponent implements OnInit, AfterViewInit {
    private destroyed$ = new Subject();

    private keyCodeMaps = {
        Enter: 13
    };

    public IDFromRoute;
    public itemData = {
        product_id: 0,
        product_stock_id: 0,
        barcode: '',
        product_name: '',
        unit: '',
        unit_price: 0,
        unit_cost: 0,
        low_stock_count: 0,
        high_stock_count: 0,
        quantity: 0,
        image_url: ''
    };

    @ViewChild('quantityInput') quantityInput;

    public addProductStockForm: FormGroup;
    public isFormValid = true;

    public formMode = 'ADD_PRODUCT_STOCK';
    public pageTitle = 'Add Product Stock';
    public actionButtonText = 'Add Product Stock';
    public quantityLabel = 'How many stock do you want to add to this product?';
    public quantityError = 'Quantity of Stock to add is required';
    public quantityDescription = 'Kindly input the quantity of stock you want to add.';

    constructor(
        private ProductsService: ProductsService,
        private ProductStocksService: ProductStocksService,
        private LossItemsService: LossItemsService,
        private ActivatedRoute: ActivatedRoute,
        private Router: Router
    ) {
    }

    ngOnInit() {
        this.IDFromRoute = this.ActivatedRoute.snapshot.params.product_id;
        this.setFormMode();
        this.buildProductStockForm();

        this.ProductsService.getItem(this.IDFromRoute, 'product_id')
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                this.itemData = response.data.product;
            });
    }

    ngAfterViewInit() {
        this.quantityInput.nativeElement.focus();
    }

    buildProductStockForm() {
        this.addProductStockForm = new FormGroup({
            quantity: new FormControl('', [Validators.required])
        });
    }

    formAction() {
        let redirectUrl = this.Router.routerState.snapshot.url;
        let indexOfRedirect = redirectUrl.indexOf('redirect');
        let decodedRedirect = indexOfRedirect !== -1 ? decodeURIComponent(redirectUrl.substring(indexOfRedirect + 9)) : null;

        if (this.formMode === 'ADD_PRODUCT_STOCK') {
            this.isFormValid = this.addProductStockForm.valid;

            if (!this.isFormValid) {
                console.log('Form Invalid', this.addProductStockForm.errors)
                return;
            }

            const productStockID = this.itemData.product_stock_id;
            let oldQuantity = this.itemData.quantity;
            let quantityToAdd = this.addProductStockForm.value.quantity;
            // @ts-ignore
            let newQuantity = parseFloat(oldQuantity) + parseFloat(quantityToAdd);
            let formData = {quantity: newQuantity};

            this.ProductStocksService.updateProductStockItem(productStockID, formData)
                .pipe(takeUntil(this.destroyed$))
                .subscribe(response => {
                    this.redirectAfterFormAction(decodedRedirect);
                });
        } else if (this.formMode === 'EDIT_PRODUCT_STOCK') {
            this.isFormValid = this.addProductStockForm.valid;

            if (!this.isFormValid) {
                console.log('Form Invalid', this.addProductStockForm.errors)
                return;
            }

            const productStockID = this.itemData.product_stock_id;
            let newQuantity = this.addProductStockForm.value.quantity;
            let formData = {quantity: newQuantity};

            this.ProductStocksService.updateProductStockItem(productStockID, formData)
                .pipe(takeUntil(this.destroyed$))
                .subscribe(response => {
                    this.redirectAfterFormAction(decodedRedirect);
                });
        } else if (this.formMode === 'INTERNAL_USE') {
            this.isFormValid = this.addProductStockForm.valid;

            if (!this.isFormValid) {
                return;
            }

            const productStockID = this.itemData.product_stock_id;
            let oldQuantity = this.itemData.quantity;
            let quantityToDeduct = this.addProductStockForm.value.quantity;
            // @ts-ignore
            let newQuantity = parseFloat(oldQuantity) - parseFloat(quantityToDeduct);
            let formData = {quantity: newQuantity};
            // @ts-ignore
            let totalPrice = parseFloat(quantityToDeduct) * parseFloat(this.itemData.unit_price);
            let productData = {
                product_id: this.itemData.product_id,
                product_stock_id: this.itemData.product_stock_id,
                quantity: quantityToDeduct,
                unit_cost: this.itemData.unit_cost,
                unit_price: this.itemData.unit_price,
                total_price: totalPrice,
                type: 'Internal Use'
            };

            forkJoin([
                this.ProductStocksService.updateProductStockItem(productStockID, formData),
                this.LossItemsService.createLossItem(productData)
            ])
                .pipe(takeUntil(this.destroyed$))
                .subscribe(([stock, lossItem]) => {
                    this.redirectAfterFormAction(decodedRedirect);
                });
        } else if (this.formMode === 'EXPIRED_BREAKAGE') {
            this.isFormValid = this.addProductStockForm.valid;

            if (!this.isFormValid) {
                return;
            }

            const productStockID = this.itemData.product_stock_id;
            let oldQuantity = this.itemData.quantity;
            let quantityToDeduct = this.addProductStockForm.value.quantity;
            // @ts-ignore
            let newQuantity = parseFloat(oldQuantity) - parseFloat(quantityToDeduct);
            let formData = {quantity: newQuantity};
            // @ts-ignore
            let totalPrice = parseFloat(quantityToDeduct) * parseFloat(this.itemData.unit_price);
            let productData = {
                product_id: this.itemData.product_id,
                product_stock_id: this.itemData.product_stock_id,
                quantity: quantityToDeduct,
                unit_cost: this.itemData.unit_cost,
                unit_price: this.itemData.unit_price,
                total_price: totalPrice,
                type: 'Expired/Breakage'
            };

            forkJoin([
                this.ProductStocksService.updateProductStockItem(productStockID, formData),
                this.LossItemsService.createLossItem(productData)
            ])
                .pipe(takeUntil(this.destroyed$))
                .subscribe(([stock, lossItem]) => {
                    this.redirectAfterFormAction(decodedRedirect);
                });
        }
    }

    quantityKeyPressHandler(event) {
        if (event.keyCode === this.keyCodeMaps.Enter) {
            this.formAction();
        }
    }

    setFormMode() {
        this.formMode = 'ADD_PRODUCT_STOCK';
        this.pageTitle = 'Add Product Stock';
        this.actionButtonText = 'Add Product Stock';
        this.quantityLabel = 'How many stock do you want to add to this product?';
        this.quantityError = 'Quantity of Stock to add is required';
        this.quantityDescription = 'Kindly input the quantity of stock you want to add.';

        if (typeof this.ActivatedRoute.snapshot.url[2] !== 'undefined') {
            const pathMode = this.ActivatedRoute.snapshot.url[2].path;

            if (pathMode === 'edit-product-stock') {
                this.formMode = 'EDIT_PRODUCT_STOCK';
                this.pageTitle = 'Modify Product Stock';
                this.actionButtonText = 'Modify Product Stock';
                this.quantityLabel = 'New Product Stock';
                this.quantityError = 'New Product Stock is required';
                this.quantityDescription = 'Kindly input the new product stock.';
            }

            if (pathMode === 'internal-use') {
                this.formMode = 'INTERNAL_USE';
                this.pageTitle = 'Internal Use';
                this.actionButtonText = 'Add Internal Use';
                this.quantityLabel = 'How many stock was used for internal use?';
                this.quantityError = 'Quantity of Stock used is required';
                this.quantityDescription = 'Kindly input the quantity of stock used.';
            }

            if (pathMode === 'expired-breakage') {
                this.formMode = 'EXPIRED_BREAKAGE';
                this.pageTitle = 'Expired / Breakage';
                this.actionButtonText = 'Add Expired / Breakage';
                this.quantityLabel = 'How many stock has expired / broken?';
                this.quantityError = 'Quantity of Stock expired / broken is required';
                this.quantityDescription = 'Kindly input the quantity of stock expired / broken.';
            }
        }
    }

    redirectAfterFormAction(redirect = null) {
        if (redirect === null) {
            this.Router.navigate(['/product-stocks']);
        } else {
            this.Router.navigate([redirect]);
        }
    }

    get fC() {
        return this.addProductStockForm.controls;
    }
}
