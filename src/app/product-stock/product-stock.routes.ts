import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AppProductStockListComponent} from "./components/app-product-stock-list.component";
import {AppAddProductStockComponent} from "./components/add-product-stock/app-add-product-stock.component";

const routes: Routes = [
    {
        path: '',
        component: AppProductStockListComponent,
        data: {
            pageTitle: 'Product Stock list',
            pageRoute: '/stocks',
            showHeader: true,
            showNav: true
        }
    },
    {
        path: 'product-stocks/:product_id/add-product-stock',
        component: AppAddProductStockComponent,
        data: {
            pageTitle: 'Add Product Stock',
            pageRoute: '/stocks',
            showHeader: true,
            showNav: true
        }
    },
    {
        path: 'product-stocks/:product_id/edit-product-stock',
        component: AppAddProductStockComponent,
        data: {
            pageTitle: 'Add Product Stock',
            pageRoute: '/stocks',
            showHeader: true,
            showNav: true
        }
    },
    {
        path: 'product-stocks/:product_id/internal-use',
        component: AppAddProductStockComponent,
        data: {
            pageTitle: 'Add Product Stock',
            pageRoute: '/stocks',
            showHeader: true,
            showNav: true
        }
    },
    {
        path: 'product-stocks/:product_id/expired-breakage',
        component: AppAddProductStockComponent,
        data: {
            pageTitle: 'Add Product Stock',
            pageRoute: '/stocks',
            showHeader: true,
            showNav: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProductStockRoutes {}
