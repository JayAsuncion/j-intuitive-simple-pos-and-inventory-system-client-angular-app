import {NgModule} from "@angular/core";
import {AppPointOfSaleComponent} from "./components/point-of-sale/app-point-of-sale.component";
import {SalesRoutes} from "./sales.routes";
import {ProductModule} from "../product/product.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {AppCartItemListComponent} from "./components/cart/app-cart-item-list.component";
import {AppCartSummaryComponent} from "./components/cart-summary/app-cart-summary.component";
import {AppSharedModule} from "../shared/shared.module";
import {OrderModule} from "../order/order.module";
import {SimpleModalModule} from "ngx-simple-modal";
import {AppSalesListComponent} from "./components/sales-list/app-sales-list.component";

@NgModule({
    imports: [
        SalesRoutes,
        ProductModule,
        FormsModule,
        CommonModule,
        AppSharedModule,
        OrderModule,
        SimpleModalModule.forRoot({container: "modal-container"})
    ],
    declarations: [
        AppPointOfSaleComponent,
        AppCartItemListComponent,
        AppCartSummaryComponent,
        AppSalesListComponent
    ],
    providers: [

    ],
    exports: [

    ]
})
export class SalesModule {}
