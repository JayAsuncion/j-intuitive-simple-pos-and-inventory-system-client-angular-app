import {Component, OnDestroy, OnInit} from "@angular/core";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {OrdersService} from "../../../order/services/orders.service";
import {SimpleModalService} from "ngx-simple-modal";
import {AppSimpleAlertModalComponent} from "../../../shared/components/app-simple-alert-modal.component";
import {AppSimpleConfirmationModalComponent} from "../../../shared/components/app-simple-confirmation-modal.component";

@Component({
    selector: 'app-sales-list',
    templateUrl: './app-sales-list.component.html',
    styleUrls: [
        'app-sales-list.component.less'
    ]
})
export class AppSalesListComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();

    public fromDate;
    public toDate;

    public groupedOrdersByOrderID = {};
    public showLoader = false;

    constructor(
        private OrdersService: OrdersService,
        private SimpleModalService: SimpleModalService
    ) {

    }

    ngOnInit() {
       this.getSales();
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    public getSales() {
        this.showLoader = true;

        this.OrdersService.getAllOrders()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                this.groupedOrdersByOrderID = {};
                this.groupOrdersByOrderID(response.data.orders);
                this.showLoader = false;
                console.log('orders', response.data.orders);
            });
    }

    private groupOrdersByOrderID(orders) {
        if (orders.length > 0) {
            orders.forEach((order) => {
                if (typeof this.groupedOrdersByOrderID[order.order_id] === "undefined") {
                    this.groupedOrdersByOrderID[order.order_id] = {
                        order_id: order.order_id,
                        subtotal: order.subtotal,
                        discount: order.discount,
                        total: order.total,
                        cash: order.cash,
                        change: order.change,
                        date_created: order.date_created,
                        order_items: []
                    };
                }

                this.groupedOrdersByOrderID[order.order_id].order_items.push(order);
            });
        }
    }

    public voidSaleConfirmation(orderID) {
        this.SimpleModalService.addModal(AppSimpleConfirmationModalComponent, {
            icon: 'info',
            iconClass: 'extra-danger',
            title: 'Void Sale',
            message: 'Are you sure you want to void this sale? THIS IS IRREVERSIBLE.',
            buttonText1: 'Yes, void it',
            buttonText2: 'No, don\'t void it'
        }).subscribe((response) => {
            // @ts-ignore
            if (response.confirm) {
                this.voidSale(orderID);
            }
        });
    }

    private voidSale(orderID) {
        this.OrdersService.voidOrder(orderID)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                this.SimpleModalService.addModal(AppSimpleAlertModalComponent, {
                    title: 'Success',
                    message: 'Sale Voided',
                    buttonText: 'Okay'
                }, {
                    closeOnClickOutside: true
                });

                this.getSales();
            });
    }
}
