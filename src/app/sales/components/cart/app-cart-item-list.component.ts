import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
    selector: 'app-cart-item-list',
    templateUrl: 'app-cart-item-list.component.html',
    styleUrls: [
        'app-cart-item-list.component.less'
    ]
})
export class AppCartItemListComponent {
    @Input('cartItems') cartItems = {};
    @Output('addCartItemQuantityEventEmitter') addCartItemQuantityEventEmitter = new EventEmitter();
    @Output('minusCartItemQuantityEventEmitter') minusCartItemQuantityEventEmitter = new EventEmitter();

    public isObjectEmpty(obj) {
        return Object.keys(obj).length === 0;
    }

    public addCartItemQuantity(barcode, count = 1) {
        this.cartItems[barcode].quantity += count;
        this.cartItems[barcode].total_price = (parseFloat(this.cartItems[barcode].quantity) * parseFloat(this.cartItems[barcode].unit_price)).toFixed(2);

        this.addCartItemQuantityEventEmitter.emit();
    }

    public minusCartItemQuantity(barcode, count = 1) {
        if (this.cartItems[barcode].quantity <= 1) {
            // TODO: Confirm if you want to delete item
            return;
        }

        this.cartItems[barcode].quantity -= count;
        this.cartItems[barcode].total_price = (parseFloat(this.cartItems[barcode].quantity) * parseFloat(this.cartItems[barcode].unit_price)).toFixed(2);

        this.minusCartItemQuantityEventEmitter.emit();
    }

    public removeCartItem(barcode) {
        delete this.cartItems[barcode];

        this.minusCartItemQuantityEventEmitter.emit();
    }

    setProductImage(product) {
        if (product.image_url) {
            return product.image_url;
        }

        return 'assets/img/product.png';
    }
}
