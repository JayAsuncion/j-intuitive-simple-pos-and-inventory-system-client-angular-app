import {AfterViewInit, Component, HostListener, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {ProductsService} from "../../../product/services/products.service";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {AppCartSummaryComponent} from "../cart-summary/app-cart-summary.component";
import {OrdersService} from "../../../order/services/orders.service";
import {SimpleModalService} from "ngx-simple-modal";
import {OrderSummaryModalComponent} from "../../../shared/components/order-summary-modal.component";
import {AppSimpleAlertModalComponent} from "../../../shared/components/app-simple-alert-modal.component";
import {AppSimpleConfirmationModalComponent} from "../../../shared/components/app-simple-confirmation-modal.component";

@Component({
    selector: 'app-point-of-sale',
    templateUrl: './app-point-of-sale.component.html',
    styleUrls: [
        'app-point-of-sale.component.less'
    ]
})
export class AppPointOfSaleComponent implements OnInit, AfterViewInit, OnDestroy {
    private destroyed$ = new Subject();
    private keyCodeMaps = {
        Enter: 13
    };

    @ViewChild('barcodeInput') barcodeInput;
    @ViewChild('quantityInput') quantityInput;
    private discountInput;
    private cashInput;
    private nextCustomerButton;

    @ViewChild(AppCartSummaryComponent) appCartSummaryComponent;

    public barcode = ''; // 4800365101017
    public quantity = 1;
    public cartItems = {};

    public subTotal = 0;
    public discount = 2;
    public total = 0;
    public cash = 0;
    public change = 0;

    // Small Letter, Capital Letter
    public posModeShortcutKeysMap = {
        barcode: [98, 66],
        quantity: [113, 81],
        discount: [100, 68],
        cash: [99, 67],
        nextCustomer: [110, 78]
    };

    public keyDownAllowedShortcutKeys = [66, 81, 68, 67, 78];

    public showBarcodeLoader = false;

    @HostListener('window:keypress', ['$event']) windowKeyPress(event) {
        this.posModeKeypressHandler(event);
    }

    constructor(
        private productsService: ProductsService,
        private ordersService: OrdersService,
        private simpleModalService: SimpleModalService
    ) {
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this.appCartSummaryComponent.discountChangeObservable.subscribe((discount) => {
            this.discount = discount;
            this.updateSummary();
        });

        this.appCartSummaryComponent.cashChangeObservable.subscribe((cash) => {
            this.cash = cash;
            this.updateSummary();
        });

        this.discountInput = this.appCartSummaryComponent.discountInputElement;
        this.cashInput = this.appCartSummaryComponent.cashInputElement;
        this.nextCustomerButton = this.appCartSummaryComponent.nextCustomerButtonElement;

        this.barcodeInput.nativeElement.focus();
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    public barcodeInputKeyPressHandler(event) {
        if (event.keyCode === this.keyCodeMaps.Enter) {
            this.getProductToCartByBarcode(this.barcode, this.quantity);
        }
    }

    public quantityInputKeyPressHandler(event) {
        if (event.keyCode === this.keyCodeMaps.Enter) {
            this.getProductToCartByBarcode(this.barcode, this.quantity);
        }
    }

    private getProductToCartByBarcode(barcode, quantity) {
        const search_field = 'barcode';
        let totalQuantity = typeof this.cartItems[barcode] !== 'undefined' ? this.cartItems[barcode].quantity : 0;
        totalQuantity += quantity;

        // TODO: Disabled Barcode Input and Add Loader
        this.showBarcodeLoader = true;

        this.productsService.getItem(barcode, search_field, {quantity: totalQuantity})
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                let product = response.data.product;
                product.quantity = totalQuantity;
                this.addProductToCartItems(product);
                this.showBarcodeLoader = false;
                setTimeout(() => {
                    this.barcodeInput.nativeElement.focus();
                });
            }, error => {
                console.log('addProductToCart', error);
            });
    }

    private addProductToCartItems(product) {
        let cartItem = {
            product_id: product.product_id,
            barcode: product.barcode,
            product_name: product.product_name,
            image_url: product.image_url,
            unit: product.unit,
            quantity: product.quantity,
            unit_price: ''
        };

        if (
            typeof product.product_stocks !== 'undefined' &&
            product.product_stocks.length > 0
        ) {
            cartItem['product_stock_id'] = product.product_stocks[0].product_stock_id
            cartItem['unit_cost'] = product.product_stocks[0].unit_cost
            cartItem['unit_price'] = product.product_stocks[0].unit_price
            cartItem['total_price'] = (parseFloat(product.quantity) * parseFloat(product.product_stocks[0].unit_price)).toFixed(2);
        }

        this.cartItems[product.barcode] = cartItem;
        this.updateSummary();

        this.resetProductInput();
    }

    private updateSummary() {
        this.subTotal = 0;
        Object.keys(this.cartItems).forEach((barcode, index) => {
            this.subTotal += parseFloat(this.cartItems[barcode].total_price);
        });
        this.total = this.subTotal - this.discount;
        this.change = (this.cash > 0 ? this.cash - this.total : 0);
    }

    private resetProductInput() {
        this.barcode = '';
        this.quantity = 1;
        this.barcodeInput.nativeElement.focus();
    }

    private posModeKeypressHandler(event) {
        const keyCode = event.keyCode;

        if (this.posModeShortcutKeysMap.barcode.indexOf(keyCode) >= 0) {
            this.barcodeInput.nativeElement.focus();
            event.preventDefault();
        }

        if (this.posModeShortcutKeysMap.quantity.indexOf(keyCode) >= 0) {
            this.quantityInput.nativeElement.focus();
            event.preventDefault();
        }

        if (this.posModeShortcutKeysMap.discount.indexOf(keyCode) >= 0) {
            this.discountInput.nativeElement.focus();
            event.preventDefault();
        }

        if (this.posModeShortcutKeysMap.cash.indexOf(keyCode) >= 0) {
            this.cashInput.nativeElement.focus();
            event.preventDefault();
        }

        if (this.posModeShortcutKeysMap.nextCustomer.indexOf(keyCode) >= 0) {
            this.nextCustomerButton.nativeElement.focus();
            event.preventDefault();
        }
    }

    public nextCustomerButtonHandler(event) {
        const isValid = this.validateBeforeNextCustomer();

        if (!isValid) {
            return;
        }

        this.ordersService.createOrder({
            customer_info: {
                discount: this.discount,
                cash: this.cash
            },
            cart_items: this.cartItems
        }).subscribe(response => {
            this.simpleModalService.addModal(OrderSummaryModalComponent, {
                title: response.message,
                message: 'Alert message!!!',
                data: response.data.order,
                buttonText: 'Next Customer'
            }).subscribe((response) => {
               this.resetPOSForm();
            });
        })
    }

    public addQuantityHandler(event) {
        this.updateSummary();
    }

    public minusQuantityHandler(event) {
        this.updateSummary();
    }

    private validateBeforeNextCustomer() : boolean {
        if (Object.keys(this.cartItems).length === 0) {
            this.simpleModalService.addModal(AppSimpleAlertModalComponent, {
                icon: 'info',
                title: 'Attention',
                message: 'Please Add Items First',
                buttonText: 'Okay'
            }, {
                closeOnClickOutside: true
            });
            return false;
        }

        if (this.cash < this.total) {
            this.simpleModalService.addModal(AppSimpleAlertModalComponent, {
                icon: 'info',
                title: 'Attention',
                message: 'Not Enough Cash',
                buttonText: 'Okay'
            }, {
                closeOnClickOutside: true
            });
            return false
        }

        return true;
    }

    private resetPOSForm() {
        this.barcode = '';
        this.quantity = 1;
        this.barcodeInput.nativeElement.focus();

        this.cartItems = {};
        this.subTotal = 0;
        this.discount = 0;
        this.total = 0;
        this.cash = 0;
        this.change = 0;

        this.appCartSummaryComponent.resetDiscountAndCash();
    }

    public resetOrders() {
        this.simpleModalService.addModal(AppSimpleConfirmationModalComponent, {
            icon: 'info',
            iconClass: 'extra-danger',
            title: 'Reset Orders',
            message: 'Are you sure you want to reset the orders?',
            buttonText1: 'Yes, reset it',
            buttonText2: 'No, don\'t reset it'
        }).subscribe((response) => {
            // @ts-ignore
            if (response.confirm) {
                this.resetPOSForm();
            }
        });
    }
}
