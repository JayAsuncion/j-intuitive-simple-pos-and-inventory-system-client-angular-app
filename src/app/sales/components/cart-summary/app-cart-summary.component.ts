import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {BehaviorSubject} from "rxjs";

@Component({
    selector: 'app-cart-summary',
    templateUrl: 'app-cart-summary.component.html',
    styleUrls: [
        'app-cart-summary.component.less'
    ]
})
export class AppCartSummaryComponent {
    @ViewChild('discountInput') discountInputElement;
    @ViewChild('cashInput') cashInputElement;
    @ViewChild('nextCustomerButton') nextCustomerButtonElement;

    @Input('keyDownAllowedShortcutKeys') keyDownAllowedShortcutKeys = [];

    public subTotal = '0.00';
    public discountModel = 0;
    public total = '0.00';
    public cashModel = 0;
    public change = '0.00';

    public discountChangeObservable = new BehaviorSubject(this.discountModel);
    public cashChangeObservable = new BehaviorSubject(this.cashModel);
    @Output('nextCustomerClickEventEmitter') nextCustomerClickEventEmitter = new EventEmitter();

    @Input('subTotal') set setSubTotal(subTotal) {
        this.subTotal = parseFloat(subTotal).toFixed(2);
    };

    @Input('total') set setTotal(total) {
        this.total = parseFloat(total).toFixed(2);
    }

    @Input('change') set setChange(change) {
        this.change = parseFloat(change).toFixed(2);
    }

    public broadcastDiscountChange(discount) {
        discount = parseFloat(discount);

        if (isNaN(discount)) {
            discount = 0;
        }

        this.discountChangeObservable.next(discount);
    }

    public broadcastCashChange(cash) {
        cash = parseFloat(cash);

        if (isNaN(cash)) {
            cash = 0;
        }

        this.cashChangeObservable.next(cash);
    }

    public broadcastNextButtonClick(event) {
        this.nextCustomerClickEventEmitter.emit();
    }

    public setCaretPositionToEnd(event) {
        let endPosition = event.target.value.length;
        endPosition = endPosition >= 0 ? endPosition : 0;

        event.target.setSelectionRange(endPosition, endPosition);
    }

    public resetDiscountAndCash() {
        this.discountModel = 0;
        this.cashModel = 0;
    }
}
