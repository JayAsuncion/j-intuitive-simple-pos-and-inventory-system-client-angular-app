import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppPointOfSaleComponent} from "./components/point-of-sale/app-point-of-sale.component";
import {AppSalesListComponent} from "./components/sales-list/app-sales-list.component";

const routes: Routes = [
    {
        path: '',
        component : AppPointOfSaleComponent,
        data: {
            pageTitle: 'Point of Sale Mode',
            pageRoute: '/',
            showHeader: true,
            showHeaderToggle: true,
            showNav: true
        }
    },
    {
        path: 'void',
        component : AppSalesListComponent,
        data: {
            pageTitle: 'Void Sales',
            pageRoute: '/',
            showHeader: true,
            showHeaderToggle: true,
            showNav: true
        }
    }
];
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SalesRoutes {}
