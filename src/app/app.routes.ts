import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

const routes = [
    {
        path: '',
        loadChildren: './sales/sales.module#SalesModule',
        data: {
            pageTitle: 'Sales Module',
            pageRoute: ''
        }
    },
    {
        path: 'pos',
        loadChildren: './sales/sales.module#SalesModule',
        data: {
            pageTitle: 'POS Mode',
            pageRoute: ''
        }
    },
    {
        path: 'products',
        loadChildren: './product/product.module#ProductModule',
        data: {
            pageTitle: 'Product Module',
            pageRoute: ''
        }
    },
    {
        path: 'product-stocks',
        loadChildren: './product-stock/product-stock.module#ProductStockModule',
        data: {
            pageTitle: 'Product Stock Module',
            pageRoute: ''
        }
    },
    {
        path: 'sales',
        loadChildren: './sales/sales.module#SalesModule',
        data: {
            pageTitle: 'Sales Module',
            pageRoute: ''
        }
    },
    {
        path: 'reports',
        loadChildren: './reports/reports.module#ReportsModule',
        data: {
            pageTitle: 'Reports Module',
            pageRoute: ''
        }
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule]
})
export class AppRoutes {}
