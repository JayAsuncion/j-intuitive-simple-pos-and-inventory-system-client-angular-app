import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {FileHandle} from "../../shared/interface/file-handler.interface";

@Injectable({
    providedIn: "root"
})

export class ImageUploaderService {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url + '/images';

    constructor(private httpClient: HttpClient) {
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    public uploadImages(images: FileHandle[], options: {folder: ''}):Observable<any> {
        let requestPayload = new FormData();
        requestPayload.append('folder', options.folder);

        for (let image of images) {
            requestPayload.append(image.file.name, image.file, image.file.name);
        }

        return this.httpClient.post(this.resourceUrl, requestPayload);
    }
}
