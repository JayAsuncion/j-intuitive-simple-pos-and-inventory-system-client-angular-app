import {Injectable, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class RouteExtractorService implements OnInit, OnDestroy {
    private destroyed$: Subject<{}> = new Subject();
    public items: Array<any> = [];
    public showHeader = true;
    public showHeaderToggle = false;
    public showNav = true;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
        this.router.events.pipe(takeUntil(this.destroyed$)).subscribe( e => {
            if (e instanceof NavigationEnd) {
                this.items = [];
                this.extract(this.router.routerState.root);
            }
        });
    }

    ngOnInit(): void {

    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    public extract(route) {
        const pageTitle = route.data.value.pageTitle;
        const pageRoute = route.data.value.pageRoute;

        if (pageTitle && pageRoute && this.items.indexOf(pageTitle) == -1) {
            if (typeof route.data.value.pageTitle === 'object') {
                for (let i = 0; i < route.data.value.pageTitle.length; i++) {
                    this.items.push({
                        pageTitle: route.data.value.pageTitle[i],
                        pageRoute: route.data.value.pageRoute[i]
                    });
                    if (route.data.value.showNav != undefined && route.data.value.showHeader != undefined) {
                        this.showHeader = route.data.value.showHeader;
                        this.showHeaderToggle = route.data.value.showHeaderToggle;
                        this.showNav = route.data.value.showNav;
                    }
                }
            } else {
                this.items.push({
                    pageTitle: route.data.value.pageTitle,
                    pageRoute: route.data.value.pageRoute
                });

                if (this.activatedRoute.snapshot.firstChild.firstChild !== null) {
                    const routeData = this.activatedRoute.snapshot.firstChild.firstChild.data;

                    if (routeData.showHeader !== undefined  && routeData.showNav !== undefined) {
                        this.showHeader = this.activatedRoute.snapshot.firstChild.firstChild.data.showHeader;
                        this.showHeaderToggle = this.activatedRoute.snapshot.firstChild.firstChild.data.showHeaderToggle;
                        this.showNav = this.activatedRoute.snapshot.firstChild.firstChild.data.showNav;
                    }
                } else  {
                    const routeData = this.activatedRoute.snapshot.firstChild.data;

                    if (routeData.showHeader !== undefined  && routeData.showNav !== undefined) {
                        this.showHeader = this.activatedRoute.snapshot.firstChild.data.showHeader;
                        this.showHeaderToggle = this.activatedRoute.snapshot.firstChild.data.showHeaderToggle;
                        this.showNav = this.activatedRoute.snapshot.firstChild.data.showNav;
                    }
                }
            }
        }

        if (route.children) {
            route.children.forEach(it => {
                this.extract(it);
            });
        }
    }
}
