import {NgModule} from "@angular/core";
import {AppHeaderComponent} from "./components/app-header.component";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        RouterModule,
        CommonModule
    ],
    declarations: [
        AppHeaderComponent
    ],
    providers: [

    ],
    exports: [
        AppHeaderComponent
    ]
})
export class CoreModule {}
