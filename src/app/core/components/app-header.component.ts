import {Component, ElementRef, Input} from "@angular/core";

@Component({
    selector: 'app-header',
    templateUrl: './app-header.component.html',
    styleUrls: [
        './app-header.component.less'
    ]
})
export class AppHeaderComponent {
    @Input() showHeaderToggle = false;

    public showHeader = true;

    constructor(private ElementRef: ElementRef) {
    }

    toggleHeaderVisibility() {
        this.showHeader = !this.showHeader;
    }
}
