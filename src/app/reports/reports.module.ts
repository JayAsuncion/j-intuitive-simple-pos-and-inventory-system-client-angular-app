import {NgModule} from "@angular/core";
import {NetProfitReportsComponent} from "./components/net-profit-reports.component";
import {ReportsRoutes} from "./reports.routes";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        ReportsRoutes,
        ReactiveFormsModule,
        FormsModule,
        CommonModule
    ],
    declarations: [
        NetProfitReportsComponent
    ],
    exports: [

    ],
    providers: [

    ]
})
export class ReportsModule {}
