import {Component, ViewChild} from "@angular/core";
import {OrdersService} from "../../order/services/orders.service";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";

@Component({
    selector: 'net-profit-reports',
    templateUrl: 'net-profit-reports.component.html',
    styleUrls: [
        'net-profit-reports.component.less'
    ]
})
export class NetProfitReportsComponent {
    private destroyed$ = new Subject();

    public fromDate;
    public toDate;

    public combinedOrderItems = {};
    public grandTotalSales = 0;
    public grandTotalCost = 0;
    public grandTotalProfit = 0;

    public showLoader = false;

    constructor(
        private OrdersService: OrdersService
    ) {
    }

    generateReport() {
        if (typeof this.fromDate === 'undefined' || typeof this.toDate === 'undefined') {
            return;
        }

        let fromDate = new Date(this.fromDate);
        fromDate.setHours(0,0,0);
        let fromDateTimestamp = fromDate.getTime();

        let toDate = new Date(this.toDate);
        toDate.setHours(23,59,59);
        let toDateTimestamp = toDate.getTime();

        this.showLoader = true;

        this.OrdersService.getOrders({fromDateTimestamp: fromDateTimestamp, toDateTimestamp: toDateTimestamp})
            .pipe(takeUntil(this.destroyed$))
            .subscribe(response => {
                console.log('response' ,response);
                this.combineSameProductsWithSameCostAndPrice(response.data.orders);
                this.showLoader = false;
            });
    }

    combineSameProductsWithSameCostAndPrice(orderItems) {
        this.combinedOrderItems = {};
        this.grandTotalSales = 0;
        this.grandTotalCost = 0;
        this.grandTotalProfit = 0;

        if (
            typeof orderItems !== 'undefined' &&
            orderItems !== null &&
            orderItems.length > 0
        ) {
            // Set Item Attributes and Map subItemTree [unit_cost][unit_price]
            orderItems.forEach((item, index) => {
                if (typeof this.combinedOrderItems[item.product_id] === "undefined") {
                    this.combinedOrderItems[item.product_id] = {
                        product_name: item.product_name,
                        unit: item.unit,
                        subItems: [],
                        subItemTree: {}
                    };
                }

                if (typeof this.combinedOrderItems[item.product_id].subItemTree[item.unit_cost] === "undefined") {
                    this.combinedOrderItems[item.product_id].subItemTree[item.unit_cost] = {};
                }

                if (typeof this.combinedOrderItems[item.product_id].subItemTree[item.unit_cost][item.unit_price] === "undefined") {
                    this.combinedOrderItems[item.product_id].subItemTree[item.unit_cost][item.unit_price] = {
                        product_name: item.product_name,
                        unit: item.unit,
                        quantity: 0
                    };
                }

                const quantity = parseFloat(item.quantity);
                this.combinedOrderItems[item.product_id].subItemTree[item.unit_cost][item.unit_price].quantity += quantity;
            });

            // Calculate Cost, Sales, Profit and Margin from Mapped Sub Item Tree
            Object.keys(this.combinedOrderItems).forEach((product_id, index, array) => {
                let subTotalOfUnit = 0;
                let subTotalOfCost = 0;
                let subTotalOfSales = 0;
                let subTotalOfProfit = 0;

                Object.keys(this.combinedOrderItems[product_id].subItemTree).forEach((unit_cost, index) => {
                    Object.keys(this.combinedOrderItems[product_id].subItemTree[unit_cost]).forEach((unit_price, index) => {
                        let subItemQuantity = parseFloat(this.combinedOrderItems[product_id].subItemTree[unit_cost][unit_price].quantity);
                        let subItemUnitCost = parseFloat(unit_cost);
                        let subItemUnitPrice = parseFloat(unit_price);
                        const cost = subItemQuantity * subItemUnitCost;
                        const sales = subItemQuantity * subItemUnitPrice;
                        const profit = sales - cost;
                        const margin = (profit / sales) * 100;

                        subTotalOfCost += cost;
                        subTotalOfSales += sales;
                        subTotalOfProfit += profit;
                        subTotalOfUnit += subItemQuantity;

                        const product_name = this.combinedOrderItems[product_id].subItemTree[unit_cost][unit_price].product_name;
                        const unit = this.combinedOrderItems[product_id].subItemTree[unit_cost][unit_price].unit;

                        let subItemObject = {
                            unit: subItemQuantity.toFixed(2) + " " + unit,
                            sales: sales.toFixed(2),
                            cost: cost.toFixed(2),
                            profit: profit.toFixed(2),
                            margin: margin + " %"
                        };

                        if (this.combinedOrderItems[product_id].subItems.length === 0) {
                            subItemObject['product_name'] = product_name;
                        }

                        this.combinedOrderItems[product_id].subItems.push(subItemObject);
                    });
                });

                // Add Subtotal Item
                let subTotalObject  = {
                    product_name: 'Sub Total',
                    unit: subTotalOfUnit.toFixed(2)  + " " + this.combinedOrderItems[product_id].unit,
                    sales: subTotalOfSales.toFixed(2),
                    cost: subTotalOfCost.toFixed(2),
                    profit: subTotalOfProfit.toFixed(2),
                    margin: ''
                }
                this.combinedOrderItems[product_id].subItems.push(subTotalObject);

                this.grandTotalSales += subTotalOfSales;
                this.grandTotalCost += subTotalOfCost;
                this.grandTotalProfit += subTotalOfProfit;
            });

            console.log(this.combinedOrderItems);
        }
    }

    isCombinedOrderItemsEmpty() {
        if (Object.keys(this.combinedOrderItems).length) {
            return false;
        }

        return true;
    }
}
