import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {NetProfitReportsComponent} from "./components/net-profit-reports.component";

const routes: Routes = [
    {
        path: '',
        component: NetProfitReportsComponent,
        data: {
            pageTitle: 'Reports',
            pageRoute: '/reports',
            showHeader: true,
            showNav: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ReportsRoutes {}
