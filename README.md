# J-Intuitive Simple Pos And Inventory System Client
Author: [Christian Jay D. Asuncion](https://gitlab.com/JayAsuncion)

---

### Environments Config
Set environments.app.api_url (Remove front slash / at the end).
```typescript
export const environment = {
    production: true,
    app: {
        client_url: 'http://pos.officialnusg.com',
        api_url: 'http://posapi.officialnusg.com'
    }
}
```

### .htaccess
There is a **.htaccess** file inside **/src** directory.
This is used to **rewrite** request to **index.php**.

## Builds
### Production
The build will be generated inside **/dist/client-prod-build**
### Local
The build will be generated inside **/dist/j-intuitive-simple-pos-and-inventory-system-client**

---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

